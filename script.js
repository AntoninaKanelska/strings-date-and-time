/*
1. Рядки можна створювати:
- через подвійні рядки "string";
- одинарні лапки 'string';
- template literals;
- конструктора String;

2. Одинарні  та подвійні  лапки використовуються для простих рядків.
Зворотні лапки використовуються для  створення багаторядкових рядків, та при використанні змінних.

3. Найчастіше рядки порівнюють за допомогою === (строге дорівнює)
також використовують  Метод localeCompare() та 4. Метод Object.is()

4. Метод Date.now() в JavaScript повертає кількість мілісекунд, що минули з початку епохи Unix  до поточного моменту.

5. Date.now() повертає кількість мілісекунд , що минули з початку епохи Unix а new Date() повертає об'єкт Date, який представляє поточну дату і час.


*/

function isPalindrome(str) {
    str = str.toLowerCase();
    str = str.replace(/[^a-z0-9]/g, '');
    let reversedStr = str.split('').reverse().join('');
    return str === reversedStr;
}


console.log(isPalindrome("Do geese see God?"));
console.log(isPalindrome("Nurse, I spy gypsies run!")); // true
console.log(isPalindrome("hello world")); // false



function isStringLengthValid(str, maxLength) {
    return str.length <= maxLength;
}

console.log(isStringLengthValid('checked string', 20));
console.log(isStringLengthValid('checked string', 10));
console.log(isStringLengthValid('short', 10));
console.log(isStringLengthValid('this is a very long string', 15));


function calculateAge() {
    const birthDateInput = prompt("Введіть вашу дату народження (yyyy-mm-dd):");

    const birthDate = new Date(birthDateInput);

    const today = new Date();

    let age = today.getFullYear() - birthDate.getFullYear();
    const monthDifference = today.getMonth() - birthDate.getMonth();

    if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }

    return age;
}

console.log(`Вам ${calculateAge()} повних років.`);


